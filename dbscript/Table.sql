create schema FindNearByFriends;
USE FindNearByFriends;
CREATE TABLE UserDetails(
	id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	name VARCHAR(300),
	number VARCHAR(50) NOT NULL,
	longitude DOUBLE,
	latitude DOUBLE,
	dob date NOT NULL,
	dobDayMonth VARCHAR(5) NOT NULL,
	lastUpdatedTime DATETIME NOT NULL DEFAULT NOW(),
	isActive TINYINT(1) NOT NULL DEFAULT '1',
	UNIQUE INDEX number_UNIQUE (number ASC),
	PRIMARY KEY(id)
);

CREATE TABLE UserFriendMapping(
	id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	userId BIGINT NOT NULL,
	friendId BIGINT NOT NULL,
	lastNearByNotifiedTime dateTime,
	lastBDayNotifiedTime dateTime,
	UNIQUE INDEX userId_UNIQUE (userId ASC),
	UNIQUE INDEX friendId_UNIQUE (friendId ASC),
	PRIMARY KEY(id)
);