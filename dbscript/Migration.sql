ALTER TABLE UserDetails ADD COLUMN dob date NOT NULL;
ALTER TABLE UserFriendMapping ADD COLUMN lastBDayNotifiedTime dateTime;
ALTER TABLE UserFriendMapping ADD COLUMN lastNearByNotifiedTime dateTime;
ALTER TABLE UserDetails ADD COLUMN dobDayMonth VARCHAR(5) NOT NULL;