package com.niranjan.findnearbyfriend.param;

import java.util.Date;
import java.util.List;

public class UserDetailsParam
{

	private String			userPhoneNumber;
	private String			userName;
	private List<String>	userFriendsPhNO;
	private double			longitude;
	private double			latitude;
	private Date			dob;
	private Boolean			isFriendMappingSync;
	private float			friendRange;

	public String getUserPhoneNumber()
	{
		return userPhoneNumber;
	}

	public void setUserPhoneNumber(String userPhoneNumber)
	{
		this.userPhoneNumber = userPhoneNumber;
	}

	public List<String> getUserFriendsPhNO()
	{
		return userFriendsPhNO;
	}

	public void setUserFriendsPhNO(List<String> userFriendsPhNO)
	{
		this.userFriendsPhNO = userFriendsPhNO;
	}

	public double getLongitude()
	{
		return longitude;
	}

	public void setLongitude(double longitude)
	{
		this.longitude = longitude;
	}

	public double getLatitude()
	{
		return latitude;
	}

	public void setLatitude(double latitude)
	{
		this.latitude = latitude;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public Boolean getIsFriendMappingSync()
	{
		return isFriendMappingSync;
	}

	public void setIsFriendMappingSync(Boolean isFriendMappingSync)
	{
		this.isFriendMappingSync = isFriendMappingSync;
	}

	public float getFriendRange()
	{
		return friendRange;
	}

	public void setFriendRange(float friendRange)
	{
		this.friendRange = friendRange;
	}

	public Date getDob()
	{
		return dob;
	}

	public void setDob(Date dob)
	{
		this.dob = dob;
	}

}
