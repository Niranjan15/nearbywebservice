package com.niranjan.findnearbyfriend.service;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.niranjan.findnearbyfriend.DBManager;
import com.niranjan.findnearbyfriend.dao.UserDetailsDAO;
import com.niranjan.findnearbyfriend.dto.IBQueryInterfact;
import com.niranjan.findnearbyfriend.dto.UserDetailsDTO;
import com.niranjan.findnearbyfriend.exception.AppException;
import com.niranjan.findnearbyfriend.param.UserDetailsParam;
import com.niranjan.findnearbyfriend.utils.ApplicationConstants;
import com.niranjan.findnearbyfriend.utils.DistanceCalculator;
import com.niranjan.findnearbyfriend.utils.FindFriendsUtils;

public class UserDetailsService
{

	private static final Logger	OUT	= LoggerFactory.getLogger(UserDetailsService.class);

	/**
	 * @param detailsParam
	 * @return
	 * @return
	 * @throws Exception
	 */
	public List<UserDetailsDTO> insertUserDetails(UserDetailsParam detailsParam) throws Exception
	{
		validate(detailsParam);
		SqlSession session = null;
		try
		{
			UserDetailsDTO userDetailsDTO = copyToDTO(detailsParam);
			session = DBManager.getInstance().getBatchSession();

			UserDetailsDAO userDetailsDAO = new UserDetailsDAO();
			userDetailsDAO.insertUserDetails(session, userDetailsDTO, true);

			if (userDetailsDTO.getId() > 0 && detailsParam.getIsFriendMappingSync())
			{
				session.delete(IBQueryInterfact.DELETE_USER_FRIEND_MAPPING_BY_USER_ID, userDetailsDTO.getId());
				if (!CollectionUtils.isEmpty(detailsParam.getUserFriendsPhNO()))
				{
					UserDetailsDTO friendDetailsDTO = null;
					for (String userPhoneNo : detailsParam.getUserFriendsPhNO())
					{
						friendDetailsDTO = new UserDetailsDTO();
						friendDetailsDTO.setNumber(userPhoneNo);

						userDetailsDAO.insertUserDetails(session, friendDetailsDTO, false);
						userDetailsDAO.insertUserFriendMapping(session, userDetailsDTO.getId(), friendDetailsDTO.getId());
					}
				}
			}
			session.commit();
			List<UserDetailsDTO> bDayFriends = getBDayFriends(userDetailsDTO);
			List<UserDetailsDTO> nearByFriends = getNearByFriends(userDetailsDTO);
			List<UserDetailsDTO> finalFriendList = new ArrayList<UserDetailsDTO>();
			if (bDayFriends != null)
			{
				finalFriendList.addAll(bDayFriends);
			}
			if (nearByFriends != null)
			{
				finalFriendList.addAll(nearByFriends);
			}
			new Thread(new NotificationUpdateService(userDetailsDTO, bDayFriends, nearByFriends)).start();
			return finalFriendList;
		}
		catch (Exception e)
		{
			if (session != null)
			{
				session.rollback();
			}
			OUT.error("Exception", e);
			throw e;
		}
		finally
		{
			if (session != null)
			{
				session.close();
			}
		}
	}

	/**
	 * @param detailsParam
	 */
	private void validate(UserDetailsParam detailsParam)
	{
		if (detailsParam.getUserPhoneNumber() == null)
		{
			throw new AppException("1001", "User Phone number is required");
		}
		if (detailsParam.getLatitude() == 0d)
		{
			throw new AppException("1003", "User corrent latitude is required");
		}
		if (detailsParam.getLongitude() == 0d)
		{
			throw new AppException("1004", "User corrent longitude is required");
		}
	}

	/**
	 * @param detailsParam
	 * @return
	 */
	private UserDetailsDTO copyToDTO(UserDetailsParam detailsParam)
	{
		UserDetailsDTO detailsDTO = new UserDetailsDTO();
		detailsDTO.setLatitude(detailsParam.getLatitude());
		detailsDTO.setLongitude(detailsParam.getLongitude());
		detailsDTO.setName(detailsParam.getUserName());
		detailsDTO.setNumber(detailsParam.getUserPhoneNumber());
		detailsDTO.setFriendRange(detailsParam.getFriendRange());
		if (detailsParam.getDob() != null)
		{
			detailsDTO.setDob(detailsParam.getDob());
			detailsDTO.setDobDayMonth(FindFriendsUtils.getDOBDayMonth(detailsParam.getDob()));
		}
		return detailsDTO;
	}

	private List<UserDetailsDTO> getNearByFriends(UserDetailsDTO userDetailsDTO)
	{
		List<UserDetailsDTO> resultAsList = null;
		try
		{
			userDetailsDTO.setLastUpdatedTime(getLastUpdatedTime());
			resultAsList = DBManager.getInstance().getResultAsList(IBQueryInterfact.GET_NEAR_BY_FRIENDS_BY_USER_ID, userDetailsDTO);
			OUT.debug("Number of near by friends found : {}", resultAsList == null ? 0 : resultAsList.size());

			if (resultAsList != null)
			{
				Iterator<UserDetailsDTO> iterator = resultAsList.iterator();
				UserDetailsDTO detailsDTO = null;
				double distance = 0;
				while (iterator.hasNext())
				{
					distance = 0;
					detailsDTO = iterator.next();
					if ((distance = DistanceCalculator.isNearByFriend(userDetailsDTO.getLatitude(), userDetailsDTO.getLongitude(), detailsDTO.getLatitude(),
							detailsDTO.getLongitude(), userDetailsDTO.getFriendRange())) == -1)
					{
						iterator.remove();
					}
					else
					{
						detailsDTO.setDistance(distance);
						setMessage(distance, detailsDTO);
					}
				}
			}
			// else
			// {
			// resultAsList = new ArrayList<UserDetailsDTO>();
			// resultAsList.addAll(getAllMappedUsers(userDetailsDTO.getNumber()));
			// }
		}
		catch (Exception e)
		{
			OUT.error(ApplicationConstants.EXCEPTION, e);
		}
		return resultAsList;
	}

	private void setMessage(double distance, UserDetailsDTO detailsDTO)
	{
		if (distance > 0)
		{
			detailsDTO.setMsg(MessageFormat.format(" near by you at distance {0}", distance));
		}
	}

	/**
	 * @param userDetailsDTO
	 * @return
	 */
	private List<UserDetailsDTO> getBDayFriends(UserDetailsDTO userDetailsDTO)
	{
		try
		{
			userDetailsDTO.setDobDayMonth(FindFriendsUtils.getDOBDayMonth(new Date()));
			userDetailsDTO.setCurrentYear(FindFriendsUtils.getCurrentYear());
			List<UserDetailsDTO> resultAsList = DBManager.getInstance().getResultAsList(IBQueryInterfact.GET_BDAY_FRINDS_BY_USER_ID, userDetailsDTO);
			OUT.debug("Number of birth day friends found : {}", resultAsList == null ? 0 : resultAsList.size());

			if (resultAsList != null)
			{
				Iterator<UserDetailsDTO> iterator = resultAsList.iterator();
				UserDetailsDTO detailsDTO = null;
				while (iterator.hasNext())
				{
					detailsDTO = iterator.next();
					detailsDTO.setIsNearByNotify(false);
					setBirthDayMessage(detailsDTO);
				}
			}
			return resultAsList;
		}
		catch (Exception e)
		{
			OUT.error(ApplicationConstants.EXCEPTION, e);
		}
		return null;
	}

	private void setBirthDayMessage(UserDetailsDTO detailsDTO)
	{
		detailsDTO.setMsg("'s birthday today");
	}

	public List<UserDetailsDTO> getAllMappedUsers(String phoneNumber)
	{
		try
		{

			UserDetailsDTO userDetailsDTO = new UserDetailsDTO();
			userDetailsDTO.setNumber(phoneNumber);
			return DBManager.getInstance().getResultAsList(IBQueryInterfact.GET_ALL_MAPPED_USERS_BY_USER_ID, userDetailsDTO);
		}
		catch (Exception e)
		{
			OUT.error(ApplicationConstants.EXCEPTION, e);
		}
		return null;
	}

	private Date getLastUpdatedTime()
	{
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, -5);
		calendar.add(Calendar.SECOND, 0);
		return calendar.getTime();
	}

	/*
	 * public static void main(String[] args) throws Exception
	 * {
	 * GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyAl369erWHEmvJr29PGTkqdn8Osb-R0Yfs");
	 * GeocodingResult[] results = GeocodingApi.geocode(context, "Bangalore vidyaranayapura karnataka").await();
	 * System.out.println(results[0].formattedAddress);
	 * }
	 */
}
