package com.niranjan.findnearbyfriend.service;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.niranjan.findnearbyfriend.DBManager;
import com.niranjan.findnearbyfriend.dto.IBQueryInterfact;
import com.niranjan.findnearbyfriend.dto.UserDetailsDTO;
import com.niranjan.findnearbyfriend.dto.UserFriendMappingDTO;
import com.niranjan.findnearbyfriend.utils.ApplicationConstants;

public class NotificationUpdateService implements Runnable
{
	private static final Logger		OUT				= LoggerFactory.getLogger(NotificationUpdateService.class);
	private List<UserDetailsDTO>	bDayFriends		= null;
	private List<UserDetailsDTO>	nearByFriends	= null;
	private UserDetailsDTO			userDetails		= null;

	public NotificationUpdateService(UserDetailsDTO userDetails, List<UserDetailsDTO> bDayFriends, List<UserDetailsDTO> nearByFriends)
	{
		this.bDayFriends = bDayFriends;
		this.nearByFriends = nearByFriends;
		this.userDetails = userDetails;
	}

	public void run()
	{
		Date lastNotifiedDate = new Date();

		UserFriendMappingDTO friendMappingDTO = new UserFriendMappingDTO();
		friendMappingDTO.setFriendId(userDetails.getId());

		if (bDayFriends != null)
		{
			friendMappingDTO.setLastBDayNotifiedTime(lastNotifiedDate);
			for (UserDetailsDTO bDayFriendUserDetails : bDayFriends)
			{
				try
				{
					friendMappingDTO.setUserId(bDayFriendUserDetails.getId());
					DBManager.getInstance().update(IBQueryInterfact.UPDATE_NOTIFIED_TIME_BY_USER_AND_FRND_ID, friendMappingDTO);
				}
				catch (Exception e)
				{
					OUT.error(ApplicationConstants.EXCEPTION, e);
				}
			}
		}
		if (nearByFriends != null)
		{
			friendMappingDTO.setLastBDayNotifiedTime(null);
			friendMappingDTO.setLastNearByNotifiedTime(lastNotifiedDate);
			for (UserDetailsDTO nearByFrndUserDetails : nearByFriends)
			{
				try
				{
					friendMappingDTO.setUserId(nearByFrndUserDetails.getId());
					DBManager.getInstance().update(IBQueryInterfact.UPDATE_NOTIFIED_TIME_BY_USER_AND_FRND_ID, friendMappingDTO);
				}
				catch (Exception e)
				{
					OUT.error(ApplicationConstants.EXCEPTION, e);
				}
			}
		}
	}

}
