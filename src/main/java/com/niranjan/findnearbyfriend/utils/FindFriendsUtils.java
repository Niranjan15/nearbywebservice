package com.niranjan.findnearbyfriend.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.niranjan.findnearbyfriend.dto.CommonResponceDTO;

public class FindFriendsUtils
{
	private static SimpleDateFormat	dateFormat	= new SimpleDateFormat("dd-MM");

	/**
	 * @param responceDTO
	 * @param errorCode
	 * @param errorMessage
	 */
	public static void setCommonResponce(CommonResponceDTO responceDTO, String errorCode, String errorMessage)
	{
		if (responceDTO != null)
		{
			responceDTO.setErrorCode(errorCode);
			responceDTO.setErrorMessage(errorMessage);
			responceDTO.setStatus(ApplicationConstants.FAILURE);
		}
	}

	/**
	 * @param dob
	 * @return
	 */
	public static String getDOBDayMonth(Date dob)
	{
		return dateFormat.format(dob);
	}

	public static String getCurrentYear()
	{
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		return String.valueOf(year);
	}
}
