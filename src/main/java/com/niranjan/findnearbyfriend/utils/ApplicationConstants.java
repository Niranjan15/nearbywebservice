package com.niranjan.findnearbyfriend.utils;

public class ApplicationConstants
{
	public static final String	FAILURE					= "FAILURE";
	public static final String	SUCCESS					= "SUCCESS";
	public static final String	EXCEPTION				= "EXCEPTION";
	public static final String	INTERNAL_ERROR_MESSAGE	= "Internal error occored";
	public static final String	INTERNAL_ERROR			= "99";

}
