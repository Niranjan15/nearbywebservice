package com.niranjan.findnearbyfriend.utils;

public interface URLConstants
{

	String	INSERT_USER_DETAILS		= "/insertuserdetails";
	String	GET_ALL_MAPPED_USERS	= "/getAllMappedUsers";
}
