package com.niranjan.findnearbyfriend.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.niranjan.findnearbyfriend.dto.CommonResponceDTO;
import com.niranjan.findnearbyfriend.dto.UserDetailsDTO;
import com.niranjan.findnearbyfriend.exception.AppException;
import com.niranjan.findnearbyfriend.param.UserDetailsParam;
import com.niranjan.findnearbyfriend.service.UserDetailsService;
import com.niranjan.findnearbyfriend.utils.ApplicationConstants;
import com.niranjan.findnearbyfriend.utils.FindFriendsUtils;
import com.niranjan.findnearbyfriend.utils.URLConstants;

@Controller
public class UserDetailsController
{

	@RequestMapping(value = URLConstants.INSERT_USER_DETAILS, method = RequestMethod.POST)
	public @ResponseBody CommonResponceDTO insertUserDetails(@RequestBody UserDetailsParam detailsParam)
	{
		CommonResponceDTO responceDTO = new CommonResponceDTO();
		try
		{
			List<UserDetailsDTO> insertUserDetails = new UserDetailsService().insertUserDetails(detailsParam);
			responceDTO.setResponce(insertUserDetails);
			responceDTO.setStatus(ApplicationConstants.SUCCESS);
		}
		catch (AppException e)
		{
			FindFriendsUtils.setCommonResponce(responceDTO, e.getErrorCode(), e.getErrorMsg());
		}
		catch (Exception e)
		{
			FindFriendsUtils.setCommonResponce(responceDTO, ApplicationConstants.INTERNAL_ERROR, ApplicationConstants.INTERNAL_ERROR_MESSAGE);
		}
		return responceDTO;
	}

	@RequestMapping(value = URLConstants.GET_ALL_MAPPED_USERS, method = RequestMethod.POST)
	public @ResponseBody CommonResponceDTO getAllMappedUser(@RequestBody UserDetailsParam detailsParam)
	{
		CommonResponceDTO responceDTO = new CommonResponceDTO();
		try
		{
			responceDTO.setResponce(new UserDetailsService().getAllMappedUsers(detailsParam.getUserPhoneNumber()));
			responceDTO.setStatus(ApplicationConstants.SUCCESS);
		}
		catch (AppException e)
		{
			FindFriendsUtils.setCommonResponce(responceDTO, e.getErrorCode(), e.getErrorMsg());
		}
		catch (Exception e)
		{
			FindFriendsUtils.setCommonResponce(responceDTO, ApplicationConstants.INTERNAL_ERROR, ApplicationConstants.INTERNAL_ERROR_MESSAGE);
		}
		return responceDTO;
	}

}
