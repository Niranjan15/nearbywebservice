package com.niranjan.findnearbyfriend.dto;

public class CommonResponceDTO implements IModel
{
	private static final long	serialVersionUID	= 1L;
	private String				errorCode;
	private String				errorMessage;
	private String				status;
	private Object				responce;

	public String getErrorCode()
	{
		return errorCode;
	}

	public void setErrorCode(String errorCode)
	{
		this.errorCode = errorCode;
	}

	public String getErrorMessage()
	{
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public Object getResponce()
	{
		return responce;
	}

	public void setResponce(Object responce)
	{
		this.responce = responce;
	}

}
