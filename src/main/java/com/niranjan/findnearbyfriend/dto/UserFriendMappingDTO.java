package com.niranjan.findnearbyfriend.dto;

import java.util.Date;

public class UserFriendMappingDTO implements IModel
{
	private static final long	serialVersionUID	= 1L;

	private long				id;
	private long				userId;
	private long				friendId;
	private Date				lastNearByNotifiedTime;
	private Date				lastBDayNotifiedTime;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public long getUserId()
	{
		return userId;
	}

	public void setUserId(long userId)
	{
		this.userId = userId;
	}

	public long getFriendId()
	{
		return friendId;
	}

	public void setFriendId(long friendId)
	{
		this.friendId = friendId;
	}

	public Date getLastNearByNotifiedTime()
	{
		return lastNearByNotifiedTime;
	}

	public void setLastNearByNotifiedTime(Date lastNearByNotifiedTime)
	{
		this.lastNearByNotifiedTime = lastNearByNotifiedTime;
	}

	public Date getLastBDayNotifiedTime()
	{
		return lastBDayNotifiedTime;
	}

	public void setLastBDayNotifiedTime(Date lastBDayNotifiedTime)
	{
		this.lastBDayNotifiedTime = lastBDayNotifiedTime;
	}

}
