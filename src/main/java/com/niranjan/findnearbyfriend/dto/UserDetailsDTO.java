package com.niranjan.findnearbyfriend.dto;

import java.util.Date;

public class UserDetailsDTO implements IModel
{
	private static final long	serialVersionUID	= 1L;

	private long				id;
	private String				name;
	private String				number;
	private double				longitude;
	private double				latitude;
	private Date				dob;
	private String				dobDayMonth;
	private Boolean				isActive;
	private Date				lastUpdatedTime;

	// NON table fields
	private float				friendRange;
	private String				msg;
	private double				distance;
	private String				currentYear;
	private Boolean				isNearByNotify		= true;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getNumber()
	{
		return number;
	}

	public void setNumber(String number)
	{
		this.number = number;
	}

	public Boolean getIsActive()
	{
		return isActive;
	}

	public void setIsActive(Boolean isActive)
	{
		this.isActive = isActive;
	}

	public double getLongitude()
	{
		return longitude;
	}

	public void setLongitude(double longitude)
	{
		this.longitude = longitude;
	}

	public double getLatitude()
	{
		return latitude;
	}

	public void setLatitude(double latitude)
	{
		this.latitude = latitude;
	}

	public Date getLastUpdatedTime()
	{
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(Date lastUpdatedTime)
	{
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public double getDistance()
	{
		return distance;
	}

	public void setDistance(double distance)
	{
		this.distance = distance;
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public float getFriendRange()
	{
		return friendRange;
	}

	public void setFriendRange(float friendRange)
	{
		this.friendRange = friendRange;
	}

	public Date getDob()
	{
		return dob;
	}

	public void setDob(Date dob)
	{
		this.dob = dob;
	}

	public String getMsg()
	{
		return msg;
	}

	public void setMsg(String msg)
	{
		this.msg = msg;
	}

	public String getDobDayMonth()
	{
		return dobDayMonth;
	}

	public void setDobDayMonth(String dobDayMonth)
	{
		this.dobDayMonth = dobDayMonth;
	}

	public String getCurrentYear()
	{
		return currentYear;
	}

	public void setCurrentYear(String currentYear)
	{
		this.currentYear = currentYear;
	}

	public Boolean getIsNearByNotify()
	{
		return isNearByNotify;
	}

	public void setIsNearByNotify(Boolean isNearByNotify)
	{
		this.isNearByNotify = isNearByNotify;
	}

}
