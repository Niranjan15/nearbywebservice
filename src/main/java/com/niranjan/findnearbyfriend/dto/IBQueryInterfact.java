package com.niranjan.findnearbyfriend.dto;

public interface IBQueryInterfact
{

	String	INSERT_USER_DETAILS							= "UserDetails.insertUserDetails";
	String	GET_USER_DETAILS_BY_NUMBER					= "UserDetails.getUserDetailsByNumber";
	String	GET_ACTIVE_USER_DETAILS						= "UserDetails.getActiveUserDetails";
	String	UPDATE_USER_DETAILS							= "UserDetails.updateUserDetails";
	String	GET_NEAR_BY_FRIENDS_BY_USER_ID				= "UserDetails.getNearByFrindsByUserId";
	String	IS_USER_DETAILS_EXISTS						= "UserDetails.isUserDetailsExist";
	String	GET_ALL_MAPPED_USERS_BY_USER_ID				= "UserDetails.getAllMappedUsersByUserId";
	String	GET_BDAY_FRINDS_BY_USER_ID					= "UserDetails.getBDayFrindsByUserId";

	String	INSERT_USER_FRIEND_MAPPING					= "UserFriendMapping.insertUserFriendMapping";
	String	DELETE_USER_FRIEND_MAPPING_BY_USER_ID		= "UserFriendMapping.deleteUserFriendMappingByUserId";
	String	UPDATE_NOTIFIED_TIME_BY_USER_AND_FRND_ID	= "UserFriendMapping.updateNotifiedTimeByUserAndFrndId";
}
