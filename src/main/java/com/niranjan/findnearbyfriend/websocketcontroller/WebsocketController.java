package com.niranjan.findnearbyfriend.websocketcontroller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

import org.atmosphere.config.service.AtmosphereHandlerService;
import org.atmosphere.cpr.ApplicationConfig;
import org.atmosphere.cpr.AtmosphereHandler;
import org.atmosphere.cpr.AtmosphereRequest;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.atmosphere.cpr.AtmosphereResponse;
import org.atmosphere.cpr.Broadcaster;
import org.atmosphere.cpr.HeaderConfig;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.niranjan.findnearbyfriend.param.UserDetailsParam;
import com.niranjan.findnearbyfriend.service.UserDetailsService;

@AtmosphereHandlerService(path = "/nearbyfrnd/insertuserdetails")
public class WebsocketController implements AtmosphereHandler
{

	private final ObjectMapper	mapper	= new ObjectMapper();

	public void destroy()
	{

	}

	public void onRequest(AtmosphereResource resource) throws IOException
	{
		try
		{
			System.out.println("invoked " + new Date(System.currentTimeMillis()));
			AtmosphereRequest request = resource.getRequest();

			lookupBroadcaster(resource, request);
			BufferedReader message = new BufferedReader(new InputStreamReader(request.getInputStream()));
			UserDetailsParam detailsParam = mapper.readValue(message, UserDetailsParam.class);
			new UserDetailsService().insertUserDetails(detailsParam);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void onStateChange(AtmosphereResourceEvent event) throws IOException
	{
		AtmosphereResource r = event.getResource();
		AtmosphereResponse res = r.getResponse();

		if (r.isSuspended())
		{
			res.getWriter().write(event.getMessage().toString());
			switch (r.transport())
			{
				case JSONP:
				case LONG_POLLING:
					event.getResource().resume();
					break;
				case WEBSOCKET:
					break;
				case STREAMING:
					res.getWriter().flush();
					break;
				default:
					break;
			}
		}
		else if (!event.isResuming())
		{
			event.broadcaster().broadcast(("say bye bye!").toString());
		}
	}

	Broadcaster lookupBroadcaster(AtmosphereResource ar, AtmosphereRequest req)
	{

		String pathInfo = ar.getRequest().getPathInfo();
		String[] decodedPath = pathInfo.split("/");
		Broadcaster b = ar.getAtmosphereConfig().getBroadcasterFactory().lookup(decodedPath[decodedPath.length - 1], false);
		if (b == null)
		{
			b = ar.getAtmosphereConfig().getBroadcasterFactory().lookup(decodedPath[decodedPath.length - 1], true);
			ar.setBroadcaster(b);

			if (req.getHeader(HeaderConfig.X_ATMOSPHERE_TRANSPORT).equalsIgnoreCase(HeaderConfig.LONG_POLLING_TRANSPORT))
			{
				req.setAttribute(ApplicationConfig.RESUME_ON_BROADCAST, Boolean.TRUE);
				ar.suspend();
			}
			else
			{
				ar.suspend(-1);
			}
		}
		return b;
	}
}