package com.niranjan.findnearbyfriend.exception;

public class AppException extends RuntimeException
{
	private static final long	serialVersionUID	= 1L;

	private String				errorCode;
	private String				errorMsg;

	public AppException(String errorCode, String errorMsg)
	{
		super(errorMsg);
		this.errorCode = errorCode;
		this.errorMsg = errorMsg;
	}

	public String getErrorCode()
	{
		return errorCode;
	}

	public String getErrorMsg()
	{
		return errorMsg;
	}

}
