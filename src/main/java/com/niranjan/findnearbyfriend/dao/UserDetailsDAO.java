package com.niranjan.findnearbyfriend.dao;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.niranjan.findnearbyfriend.dto.IBQueryInterfact;
import com.niranjan.findnearbyfriend.dto.UserDetailsDTO;
import com.niranjan.findnearbyfriend.dto.UserFriendMappingDTO;

public class UserDetailsDAO
{
	private static final Logger	OUT	= LoggerFactory.getLogger(UserDetailsDAO.class);

	/**
	 * @param session
	 * @param userDetailsDTO
	 * @param isUser
	 */
	public void insertUserDetails(SqlSession session, UserDetailsDTO userDetailsDTO, boolean isUser)
	{
		userDetailsDTO.setId(isExist(session, userDetailsDTO));
		if (userDetailsDTO.getId() <= 0)
		{
			session.insert(IBQueryInterfact.INSERT_USER_DETAILS, userDetailsDTO);
			session.flushStatements();
			OUT.debug("Created new user, user id : {}", userDetailsDTO.getId());
		}
		else if (isUser && userDetailsDTO.getLatitude() > 0 && userDetailsDTO.getLongitude() > 0)
		{
			updateUserDetails(session, userDetailsDTO);
			session.flushStatements();
			OUT.debug("Updated userdetails for user id : {}", userDetailsDTO.getId());
		}

	}

	/**
	 * @param session
	 * @param userId
	 * @param friendId
	 */
	public void insertUserFriendMapping(SqlSession session, long userId, long friendId)
	{
		UserFriendMappingDTO mappingDTO = new UserFriendMappingDTO();
		mappingDTO.setUserId(userId);
		mappingDTO.setFriendId(friendId);
		session.insert(IBQueryInterfact.INSERT_USER_FRIEND_MAPPING, mappingDTO);
		OUT.debug("Mappend friend id : {} to user id : {}", friendId, userId);
	}

	/**
	 * @param session
	 * @param userDetailsDTO
	 * @return
	 */
	public Long isExist(SqlSession session, UserDetailsDTO userDetailsDTO)
	{
		Long userId = (Long) session.selectOne(IBQueryInterfact.IS_USER_DETAILS_EXISTS, userDetailsDTO);
		if (userId == null)
		{
			return 0l;
		}
		return userId;
	}

	/**
	 * @param session
	 * @param userDetailsDTO
	 */
	public void updateUserDetails(SqlSession session, UserDetailsDTO userDetailsDTO)
	{
		session.update(IBQueryInterfact.UPDATE_USER_DETAILS, userDetailsDTO);
	}
}
